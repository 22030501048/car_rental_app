import 'package:flutter/material.dart';

class Screen2 extends StatefulWidget {
  const Screen2({super.key});

  @override
  State<Screen2> createState() => _Screen2State();
}

class _Screen2State extends State<Screen2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(),
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        title: Row(

          children: [
            Expanded(
              child: Column(
                children: [
                  Text('Food'),
                ],
              ),
            ),
            Column(
              children: [
                Icon(Icons.person),
              ],
            )
          ],
        ),

      ),
      body: Container(
        padding: EdgeInsets.all(25),
        child: Column(
          children: [
            Text('Find your favourite food in your city', style: TextStyle(fontSize: 30, fontWeight: FontWeight.w600),),
            SizedBox(
              height: 10,
            ),
            Row(
              children: [
                Expanded(
                  child: TextField(
                    decoration: InputDecoration(
                      border: OutlineInputBorder(
                        borderSide: BorderSide(width: 3),
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: Colors.grey),
                      ),
                      enabledBorder: OutlineInputBorder(

                        borderSide: BorderSide(color: Colors.blue),
                      ),
                      hintText: "Search",
                    ),
                  ),
                ),
                SizedBox(
                  width: 10,
                ),
                Container(
                    padding: EdgeInsets.all(8),
                    color: Colors.deepOrange,
                    child: Icon(Icons.search_rounded, size: 35, color: Colors.white,))
              ],
            ),
            SizedBox(
              height: 20,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(children: [Container(
                    width: 100,
                    height: 100,
                    child: Image.asset('asset/images/1.jpg', fit: BoxFit.fill,)),
                  Text('Food',)],),
                Column(children: [Container(
                    width: 100,
                    height: 100,
                    child: Image.asset('asset/images/1.jpg', fit: BoxFit.fill,)),
                  Text('Food',)],),
                Column(children: [Container(
                    width: 100,
                    height: 100,
                    child: Image.asset('asset/images/1.jpg', fit: BoxFit.fill,)),
                  Text('Food',)],),
                Column(children: [Container(
                    width: 100,
                    height: 100,
                    child: Image.asset('asset/images/1.jpg', fit: BoxFit.fill,)),
                  Text('Food',)],),
              ],
            )
          ],

        ),

      ),
    );
  }
}
